package main

/**
 binary tree traversal (in order)
 Depth first search DFS

      a
    b   c
   d e f g

output:
dbeafcg

*/

type TreeNode struct {
	Val   int
	Left  *TreeNode
	Right *TreeNode
}

func inorderTraversal(root *TreeNode) []int {
	stack := []int{}
	return subTree(root, stack)
}

func subTree(node *TreeNode, stack []int) []int {
	if node == nil {
		return stack
	}
	if node.Left != nil {
		stack = subTree(node.Left, stack)
	}
	stack = append(stack, node.Val)
	if node.Right != nil {
		stack = subTree(node.Right, stack)
	}
	return stack
}
