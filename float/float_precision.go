package main

import "fmt"

func main() {
	var f32 float32 = 16777216.0
	var floatResultA float32 = f32 + 1
	var floatResultB float32 = floatResultA + 1
	var floatResultC float32 = floatResultB + 1
	// all the values are the same
	fmt.Printf("float result a: %f \n", floatResultA)
	fmt.Printf("float result a: %f \n", floatResultB)
	fmt.Printf("float result a: %f \n", floatResultC)
}
