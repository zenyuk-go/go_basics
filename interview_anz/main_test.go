package main

import (
	"testing"
)

func TestGetAllMatchingCustomersWithSameAccounts1(t *testing.T) {
	accountsByCustomer := map[int]Accounts{
		1: {101, 103},
		2: {101, 103},
		3: {107},
		8: {105},
		9: {107},
		11: {101, 103},
	}
	result := getAllMatchingCustomersWithSameAccounts(accountsByCustomer, 1)
	if result[1] != 2 {
		t.Fail()
	}
}

func TestGetAllMatchingCustomersWithSameAccounts3(t *testing.T) {
	accountsByCustomer := map[int]Accounts{
		1: {101, 103},
		2: {101, 103},
		3: {107},
		8: {105},
		9: {107},
		11: {101, 103},
	}
	result := getAllMatchingCustomersWithSameAccounts(accountsByCustomer, 3)
	if result[1] != 9 {
		t.Fail()
	}
}

func TestGetAllMatchingCustomersWithSameAccounts5(t *testing.T) {
	accountsByCustomer := map[int]Accounts{
		1: {101, 103},
		2: {101, 103},
		3: {107},
		8: {105},
		9: {107},
		11: {101, 103},
	}
	result := getAllMatchingCustomersWithSameAccounts(accountsByCustomer, 5)
	if len(result) > 1 {
		t.Fail()
	}
}

func TestLinkAccounts(t *testing.T) {
	input := getInput()
	accounts, _ := link(input)
	if len(accounts) != 4 {
		t.Fail()
	}
	if c := accounts[101][1]; c != 2 {
		t.Fail()
	}
	if c := accounts[103][0]; c != 1 {
		t.Fail()
	}
	if c := accounts[107][1]; c != 9 {
		t.Fail()
	}
}

func TestLinkCustomers(t *testing.T) {
	input := getInput()
	_, customers := link(input)
	if len(customers) != 6 {
		t.Fail()
	}
	if a := customers[1][1]; a != 103 {
		t.Fail()
	}
	if a := customers[2][0]; a != 101 {
		t.Fail()
	}
	if a := customers[9][0]; a != 107 {
		t.Fail()
	}
}

func TestGetCustomersWithSameAccounts(t *testing.T) {
	input := getInput()
	accounts, customers := link(input)
	result := groupCustomersWithSameAccounts(accounts, customers)
	if len(result) != 2 {
		t.Fail()
	}
	if result[0][0] != 1 || result[0][1] != 2 {
		t.Fail()
	}
	if result[1][0] != 3 || result[1][1] != 9 {
		t.Fail()
	}
}