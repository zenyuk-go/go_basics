package main

import (
    "fmt"
)

func main() {
    input := getInput()
    accounts, customers := link(input)
    result := groupCustomersWithSameAccounts(accounts, customers)
    printGroupedCustomersAndAccounts(result, customers)
}

// groupCustomersWithSameAccounts create shards of customers' groups united by criteria of having the same set of accounts
func groupCustomersWithSameAccounts(accounts map[int]Customers, customers map[int]Accounts) []Customers {
    matchedCustomers := []Customers{} // customers who's all accounts matched so far
    for _, accCustomers := range accounts {
        for _, customerId := range accCustomers {
            group := getAllMatchingCustomersWithSameAccounts(customers, customerId)
            if len(group) < 2 {
                continue // skipping groups with a single customer
            }
            if !group.hasMatchingCustomerGroup(matchedCustomers) {
                matchedCustomers = append(matchedCustomers, group)
            }
        }
    }
    return matchedCustomers
}

func printGroupedCustomersAndAccounts(groupedCustomers []Customers, accountsByCustomer map[int]Accounts) {
    fmt.Printf("customers grouped by shared accounts:\n")
    for _, group := range groupedCustomers {
        first := true
        fmt.Printf("[")
        for _, customer := range group {
            if first {
                fmt.Printf("%d", customer)
                first = false
            } else {
                fmt.Printf(", %d", customer)
            }
        }
        fmt.Printf("] => [")
        firstCustomerInGroup := group[0]
        first = true
        for _, acc := range accountsByCustomer[firstCustomerInGroup] {
            if first {
                fmt.Printf("%d", acc)
                first = false
            } else {
                fmt.Printf(", %d", acc)
            }
        }
        fmt.Printf("]\n")
    }
}