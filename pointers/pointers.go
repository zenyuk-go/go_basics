package main

import (
	"fmt"
)

func main() {
	a := []int{0, 0, 0}
	arr(&a)
	m := map[int]int{
		0: 0,
		1: 0,
		2: 0,
	}
	mapp(m)
	fmt.Printf("arr:%v, map:%v\n", a, m)
	
	s := abc{
		Arr: make([]int, 3),
		Mapp: map[int]int{},
	}
	s.change()
	fmt.Printf("struct:%v", s)
}

func arr(a *[]int) {
	(*a)[0] = 1
	*a = append(*a, 7) // won't work w/o *[]int in the arg
}

func mapp(m map[int]int) {
	m[0] = 1
	m[101] = 7
}

type abc struct {
	Arr []int
	Mapp map[int]int
}

func(a abc) change() {
	a.Arr[0] = 1
	a.Arr = append(a.Arr, 7) // won't work w/o *abc in the receiver
	
	a.Mapp[0] = 1
	a.Mapp[101] = 7
}