package main

import (
	"fmt"
)

type A interface {
	MethodA()
	Close()
}

type B interface {
	MethodB()
	Close()
}

type C interface {
	A
	B
}

type ImplementC struct {
	Size int
}

func (i *ImplementC) MethodA() {
	fmt.Printf("\n A()")
}

func (i *ImplementC) MethodB() {
	fmt.Printf("\n B()")
}

func (i *ImplementC) Close() {
	fmt.Printf("\n Close()")
}

func main() {
	var c C
	c = createC()
	c.MethodA()
	c.MethodB()
	c.Close()
}

func createC() *ImplementC {
	return new(ImplementC)
}