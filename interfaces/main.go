package main

import "fmt"

type MyInterface interface {
	fly()
}

type Plane struct {
	speed int
}

func (p *Plane) slowdown() {
	p.speed -= 10
	fmt.Printf("plane is slowing down to speed %d \n", p.speed)
}

func (p *Plane) fly() {
	fmt.Printf("plane is flying at speed %d \n", p.speed)
}

func main() {
	//p := Plane{555}
	//p.fly()

	// version without `&` will work if we replace receivers (line 13 and 18) to `(p Plane)`
	//var i MyInterface = Plane{777}

	var i MyInterface = &Plane{777}
	i.fly()
}
