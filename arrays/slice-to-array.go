package main

import "fmt"

func main() {
	s := []string{"a", "b", "c"}
	var a [3]string
	copy(a[:], s)
	fmt.Printf("%v", a)
}