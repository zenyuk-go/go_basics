package main

import (
	"fmt"
	"strconv"
)

/*
 * Complete the 'countCounterfeit' function below.
 *
 * The function is expected to return an INTEGER.
 * The function accepts STRING_ARRAY serialNumber as parameter.
 */

func countCounterfeit(serialNumber []string) int32 {
	// Write your code here

	var result int32
	for _, sn := range serialNumber {
		if isSerialNumberValid(sn) {
			valueIsValid, value := tryGetValue(sn)
			if valueIsValid {
				result += value
			}
		}
	}
	return result
}

func isSerialNumberValid(serialNumber string) bool {
	return isLengthValid(serialNumber) &&
		isPrefixValid(serialNumber) &&
		isYearValid(serialNumber) &&
		isPostfixValid(serialNumber)
}

func isLengthValid(serialNumber string) bool {
	l := len(serialNumber)
	return l >= 10 && l <= 12
}

func isPrefixValid(serialNumber string) bool {
	for i := 0; i < 3; i++ {
		if !isValidCapitalLetter(serialNumber[i]) {
			return false
		}
	}
	return true
}

func isPostfixValid(serialNumber string) bool {
	length := len(serialNumber)
	if isValidCapitalLetter(serialNumber[length-1]) {
		return true
	}
	return false
}

func isYearValid(serialNumber string) bool {
	for i := 3; i < 7; i++ {
		if !isValidDigit(serialNumber[i]) {
			return false
		}
	}
	year, err := strconv.Atoi(serialNumber[3:7])
	if err != nil {
		return false
	}
	return year >= 1900 && year <= 2019
}

func tryGetValue(serialNumber string) (bool, int32) {
	denominations := []int32{10, 20, 50, 100, 200, 500, 1000}
	length := len(serialNumber)

	if !isValidDigit(serialNumber[7]) || !isValidDigit(serialNumber[8]) {
		return false, 0
	}

	valueDecimalPositionsCount := 2

	if isValidDigit(serialNumber[9]) {
		valueDecimalPositionsCount = 3
		if isValidDigit(serialNumber[10]) {
			valueDecimalPositionsCount = 4
		}
	}

	// variable length depends only on currency denomination length
	if length-valueDecimalPositionsCount != 8 {
		return false, 0
	}

	value, err := strconv.Atoi(serialNumber[7 : 7+valueDecimalPositionsCount])
	if err != nil {
		return false, 0
	}
	for _, d := range denominations {
		if int32(value) == d {
			return true, d
		}
	}
	return false, 0
}

func isValidDigit(d byte) bool {
	// In ASCII 0 has the index 48 and 9 has the index 57
	asciiIndex := int(d)
	return asciiIndex >= 48 && asciiIndex <= 57
}

func isValidCapitalLetter(l byte) bool {
	// In ASCII A=65 Z=90
	asciiIndex := int(l)
	return asciiIndex >= 65 && asciiIndex <= 90
}

func main() {
	fmt.Printf("result: %v\n", countCounterfeit([]string{"ABB19991000Z"}))
}
