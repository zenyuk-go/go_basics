package main

func main() {
	//a := uint8(1)
	//a = ^a
	//println(a)

	x := uint8(9) // 8(2^3) + 1(2^0)
	y := uint8(255)

	z := x &^ y
	println(z) // 0

	zz := y &^ x
	println(zz) // 246 (255 - 8 - 1)

	//s := fmt.Sprintf("%b", zz)
}
