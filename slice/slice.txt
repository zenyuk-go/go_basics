remove elements from slice inside loop (in range over)
==
s := []int{1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
isValid := func(x int) bool { return x%3 == 0 }

i := 0 // output index
for _, x := range s {
    if isValid(x) {
        // copy and increment index
        s[i] = x
        i++
    }
}
s = s[:i]
fmt.Println(s)


create slice of a given size
==
c := make([]string, 5)


convert slice to array
==
copy(arr[:], slice)

// alternatively for Go 1.17+
s := make([]byte, 2, 4)
s0 := (*[0]byte)(s)      // s0 != nil
s1 := (*[1]byte)(s[1:])  // &s1[0] == &s[1]
s2 := (*[2]byte)(s)      // &s2[0] == &s[0]



