download
==
https://golang.org/doc/install
e.g. https://golang.org/dl/go1.16.5.linux-amd64.tar.gz


prepare folder
==
cd /usr/local
sudo mkdir /usr/local/go
sudo chown YOU go
sudo chgrp YOU go


extract
==
cd ~/Downloads
tar -C /usr/local -xzf go1.16.5.linux-amd64.tar.gz


update $PATH
==
export PATH=$PATH:/usr/local/go/bin
