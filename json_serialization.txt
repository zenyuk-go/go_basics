convert interface{} to the Type via bytes
==
emptyInterfceType := bla // interface{} type

// convert to bytes
bytes, err := json.Marshal(table)
..

// convert bytes to the Type, e.g. slice of YourType
var result []YourType
err = json.Unmarshal(bytes, &result)
..
