// same in bash: https://gitlab.com/zenyuk-linux/bash/-/blob/master/bash_scripts/find_duplicates.sh
package main

import (
    "os"
    "strings"
    "sort"
)

func main() {
    file := "temp.txt"

    data, _ := os.ReadFile(file)
    str := string(data)
    splited := strings.Split(str, ";")
    sort.Strings(splited)
    prev := ""
    for _, v := range splited {
        if v == prev {
            println("duplicate: " + v)
        }
        prev = v
    }
}
