// my benchmark
// to run: 
// go test -test.bench .

package main

import (
	"testing"
	"time"
)

func BenchmarkBefore(t *testing.B) {
	for i := 0; i < t.N; i++ {
		x := 2 
		x = x ^ 3 + x * x 
		time.Sleep(40)
	}
}
func BenchmarkAfter(t *testing.B) {
	for i := 0; i < t.N; i++ {
		x := 3
		x = x * 3
	}
}
