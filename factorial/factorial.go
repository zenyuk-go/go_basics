package main

import (
	//"time"
	"math/big"
	//"fmt"
	"fmt"
	"time"
)

func main() {
	startTime := time.Now()
	res := calculate(125000)
	finishTime := time.Now()

	fmt.Println("Millisecs", finishTime.Sub(startTime).Nanoseconds())
	fmt.Println("Seconds:", finishTime.Sub(startTime).Seconds())
	fmt.Println("Result:", res)
	fmt.Println("Final duration:", time.Now().Sub(startTime).Seconds())
}

func calculate(x int64) *big.Int {
	result := big.NewInt(1)
	var counter int64 = 1

	for counter <= x {
		result.Mul(big.NewInt(counter), result)
		counter++
	}

	return result
}