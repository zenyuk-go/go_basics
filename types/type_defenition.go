package main

import "fmt"

type SpecialMap map[string]bool

func main() {
	myMap := getAutoConverted()
	fmt.Printf("map: %v\n", myMap)
}

// works
func getAutoConverted() SpecialMap {
	m := map[string]bool{}
	m["bla"] = true
	return m
}

func getRedefinedMap() SpecialMap {
	m := SpecialMap{}
	m["bla"] = false
	return m
}