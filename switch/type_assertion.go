package main

type MyInt interface {
	GetValue() int
}

type I int

func (i *I) GetValue() int {
	return int(*i)
}

func main() {
	var a MyInt
	var i I = 5

	// commenting out this line, the switch will fall in the default
	a = &i

	switch a.(type) {
	case MyInt:
		println("a is MyInt")

	// moving this block above, will fall into this case instead
	case *I:
		println("a is *I")

	default:
		println("a type is unknown")
	}
}
