package main

import "testing"

func TestOrderMapByValue(t *testing.T) {
	// arrange
	m := map[string]int{
		"third":  4,
		"first":  2,
		"second": 3,
	}

	// act
	orderedKeys := orderMapByValue(m)

	// assert
	if orderedKeys[0] != "first" ||
		orderedKeys[1] != "second" ||
		orderedKeys[2] != "third" {
		t.Fail()
	}
}

func BenchmarkHandmadeSort(b *testing.B) {
	// arrange
	m := map[string]int{
		"third":  4,
		"first":  2,
		"second": 3,
	}

	// act
	for i := 0; i < b.N; i++ {
		orderMapByValue(m)
	}
}


func BenchmarkStandardSort(b *testing.B) {
	// arrange
	m := map[string]int{
		"third":  4,
		"first":  2,
		"second": 3,
	}

	// act
	for i := 0; i < b.N; i++ {
		usingStdSort(m)
	}
}