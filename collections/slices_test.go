package main

import "testing"

func TestSlices(t *testing.T) {
	// arrange
	list := []string{"bla", "lab", "kar"}

	// act
	for index, str := range list {
		if str == "lab" {
			list = append(list[:index], list[index+1:]...)
		}
	}

	// assert
	for _, str := range list {
		if str == "lab" {
			t.Fail()
		}
	}
}
