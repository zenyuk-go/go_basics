package main

import "testing"

func TestMapWithVarFails(t *testing.T) {
	var m map[string]int
	// expecting to panic
	m["a"] = 1
}

func TestMapWithMake(t *testing.T) {
	var m = make(map[string]int)
	m["a"] = 1
	if m["a"] != 1 {
		t.Fail()
	}
}

func TestMapWithCurly(t *testing.T) {
	m := map[string]int{}
	m["a"] = 1
	if m["a"] != 1 {
		t.Fail()
	}
}
