package interview_hcl

import (
	"fmt"
	"io/fs"
	"io/ioutil"
)

func main() {
	fileInfos, err := ioutil.ReadDir("/bla")
	totalBytes := readDir(fileInfos)
	fmt.Printf("total size of the folder in bytes: %d\n", totalBytes)
}

func readDir(files []fs.FileInfo) int64 {
	var totalBytes int64
	for _, f := range files {
		if !f.IsDir() {
			totalBytes += f.Size()
		} else {
			fileInfos, err1 := ioutil.ReadDir("/bla")
			if err1 != nil {
				panic(err1)
			}
			b := readDir(fileInfos)
			totalBytes += b
		}
	}
	return totalBytes
}
