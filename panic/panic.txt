recover from panic
==
func bla() {
    defer func() {
        if err := recover(); err != nil {
            // do something
        }
    }()
    // rest of the bla


