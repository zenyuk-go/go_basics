package main

func main() {
	true := false
	if true == false {
		println("true == false")
	}

	int := false
	if int == false {
		println("int == false")
	}

	/*	Keywords:
	break        default      func         interface    select
	case         defer        go           map          struct
	chan         else         goto         package      switch
	const        fallthrough  if           range        type
	continue     for          import       return       var
	*/
}