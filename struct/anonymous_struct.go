package main

import "fmt"

func main() {
	// anonymous struct
	a := struct {
		A string
		B int
	}{"abc", 123}

	a.A = "bla"
	a.B = 5

	fmt.Printf("%v\n", a)
}
