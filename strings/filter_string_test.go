package main

import (
	"reflect"
	"testing"
)

func TestSolve(t *testing.T) {
	// arrange
	expected := []string{"nice", "guy", "corner"}

	// act
	actual := solve(phrase)

	// assert
	if !reflect.DeepEqual(expected, actual) {
		t.Errorf("Not correct solver")
	}
}
