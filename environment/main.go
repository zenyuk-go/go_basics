package main

import (
	"flag"
	"os"
)

var myBuildTimeConfigValue string

// run like:
// MYENV=23 ./environment -myRuntimeFlag hahahaha
func main() {
	// 1. build time flag
	// go build -ldflags "-X main.myBuildTimeConfigValue=bla"
	println(myBuildTimeConfigValue)

	// 2. environment variable
	envVar, ok := os.LookupEnv("MYENV")
	if !ok {
		println("env var not set")
	}
	println(envVar)

	// 3. run time flag
	runtimeFlagValue := flag.String("myRuntimeFlag", "wow_works", "bla bla bla")
	flag.Parse()
	println(*runtimeFlagValue)
}
