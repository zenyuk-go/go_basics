package main

import "fmt"

func main() {
	ch := make(chan int, 1000)
	exit := make(chan struct{})
	go receiveAndBla(ch, exit)
	go receiveAndKu(ch, exit)
	for i := 1; i <= 2000; i++ {
		ch <- i
	}
	<-exit
}

func receiveAndBla(ch <-chan int, exit chan<- struct{}) {
	for i := range ch {
		fmt.Printf("bla %d\n", i)
		if i == 2000 {
			exit <- struct{}{}
		}
	}
}

func receiveAndKu(ch <-chan int, exit chan<- struct{}) {
	for i := range ch {
		fmt.Printf("ku %d\n", i)
		if i == 2000 {
			exit <- struct{}{}
		}
	}
}
