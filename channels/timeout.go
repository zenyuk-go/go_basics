package main

import (
	"fmt"
	"time"
)

// Report mock structure
type Report struct {
	name  string
	valid bool
}

func main() {
	ch := make(chan Report, 1)
	go reportGenerator(ch)
	select {
	case report := <-ch:
		fmt.Printf("report %s arrived\n", report.name)
	// timeout
	case <-time.After(2 * time.Second):
		fmt.Println("timed out")
	}
}

func reportGenerator(ch chan<- Report) {
	time.Sleep(3 * time.Second)

	report := Report{
		name:  "report1",
		valid: true,
	}

	ch <- report
}
