package main

func unbuffered() {
	c := make(chan struct{})
	c <- struct{}{}
	<-c
	//fatal error: all goroutines are asleep - deadlock!

	println("exit")
}

func buffered() {
	c := make(chan struct{}, 1)
	c <- struct{}{}
	<-c
	// OK
	println("exit")
}

func unbuffered_no_receiver_before_send() {
	c := make(chan struct{})
	c <- struct{}{}
	go func() {
		<-c
		//fatal error: all goroutines are asleep - deadlock!

		println("read from c")
	}()
	println("exit")
}

func main() {
	c := make(chan struct{})
	go func() {
		<-c
		// OK
		println("read from c")
	}()
	c <- struct{}{}
	println("exit")
}
