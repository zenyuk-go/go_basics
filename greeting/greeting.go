// Package greeting - put this file to go/src
package greeting

import "fmt"

// Hello - prints message
func Hello() {
	fmt.Println("Hello!")
}

// Hi - prints short message
func Hi() {
	fmt.Println("Hi!")
}
