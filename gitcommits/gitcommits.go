// get number of commits in git repo in 3 years (2015-2017)

package main

import (
    "bufio"
    "fmt"
    "os"
    "os/exec"
    "strings"
)
func main() {
    // get paths
    paths := getProjectPaths()
    for _, s := range paths {
        params := []string{"log", "--pretty=format:'%cd'", "--", s}
        cmdOut, err := exec.Command("git", params...).Output()
        if err != nil {
            fmt.Println(err)
            os.Exit(1)
        }
        log := string(cmdOut)
        count := count3yCommits(log)
        fmt.Println(count, s)
    }
}
func count3yCommits(commitLog string) int {
    result := 0
    result += strings.Count(commitLog, " 2015 ")
    result += strings.Count(commitLog, " 2016 ")
    result += strings.Count(commitLog, " 2017 ")
    return result
}
func getProjectPaths() [506]string {
    f, _ := os.Open("/home/user1/allProj.txt")
    defer f.Close()
    var result [506]string
    counter := 0
    scanner := bufio.NewScanner(f)
    for scanner.Scan() {
        path := scanner.Text()
        path = path[9:len(path)]
        lastSlash := strings.LastIndex(path, "/")
        path = path[:lastSlash]
        result[counter] = path
        counter++
    }
    return result
}
